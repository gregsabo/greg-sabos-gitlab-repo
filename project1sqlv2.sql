CREATE TABLE ers_reimbursement_status(
	reimb_status_id SERIAL PRIMARY KEY
	, reimb_status VARCHAR(10) NOT NULL
);

CREATE TABLE ers_reimbursement_type(
	reimb_type_id SERIAL PRIMARY KEY
	, reimb_type VARCHAR(10) NOT NULL
);

CREATE TABLE ers_user_roles(
	ers_user_role_id SERIAL PRIMARY KEY
	, user_role VARCHAR(20) NOT NULL 
);

CREATE TABLE ers_users(
	ers_users_id SERIAL PRIMARY KEY
	, ers_username VARCHAR(50) UNIQUE NOT NULL 
	, ers_password VARCHAR(50) NOT NULL 
	, user_first_name VARCHAR(100) NOT NULL 
	, user_last_name VARCHAR(100) NOT NULL 
	, user_email VARCHAR(150) UNIQUE NOT NULL
	, user_role_id INTEGER NOT NULL 
	, FOREIGN KEY (user_role_id) REFERENCES ers_user_roles (ers_user_role_id)
);


CREATE TABLE ers_reimbursement(
	reimb_id SERIAL PRIMARY KEY 
	, reimb_amount NUMERIC NOT NULL --change this to numeric
	, reimb_submitted TIMESTAMP NOT NULL 
	, reimb_resolved TIMESTAMP
	, reimb_description VARCHAR(250)
	, reimb_author INTEGER NOT NULL 
	, reimb_resolver INTEGER
	, reimb_status_id INTEGER NOT NULL 
	, reimb_type_id INTEGER NOT NULL 
	, FOREIGN KEY (reimb_author) REFERENCES ers_users (ers_users_id)
	, FOREIGN KEY (reimb_resolver) REFERENCES ers_users (ers_users_id)
	, FOREIGN KEY (reimb_status_id) REFERENCES ers_reimbursement_status (reimb_status_id)
	, FOREIGN KEY (reimb_type_id) REFERENCES ers_reimbursement_type (reimb_type_id)
);







--DQL,DDL SELECT 














SELECT * FROM ers_users;
SELECT * FROM ers_reimbursement;
SELECT * FROM ers_reimbursement_status;
SELECT * FROM ers_user_roles;
SELECT * FROM ers_reimbursement_type;








INSERT INTO ers_users(
	ers_username, ers_password
	, user_first_name, user_last_name
	, user_email, user_role_id) 
VALUES (
	'bat', 'man'
	, 'Bat', 'Man'
	, 'batmat@batman.com', 1);


--b
INSERT INTO ers_users(
	ers_username, ers_password
	, user_first_name, user_last_name
	, user_email, user_role_id) 
VALUES (
	'man', 'bat'
	, 'Man', 'Bat'
	, 'manbat@manbat.com', 2);

