package controller.services;

import java.util.List;

import controller.dao.FridgeDAO;
import controller.dao.FridgeDAOImpl;
import controller.services.FridgeService;
import model.FoodItem;
import model.Fridge;
import model.HealthInspector;

public class FridgeServiceImpl implements FridgeService {

	FridgeDAO fridgeDAO = new FridgeDAOImpl();
	
	@Override//1
	public boolean saveFridge(Fridge fridge) {
		
		return fridgeDAO.saveFridge(fridge);
	}

	@Override//2
	public boolean deleteFridge(String fridgeName) {
		
		return fridgeDAO.deleteFridge(fridgeName);
	}

	@Override//3
	public boolean updateFridge(Fridge fridge) {
		
		return false;
	}

	@Override//4
	public List<Fridge> getAllFridges() {
		
		return fridgeDAO.getAllFridges();
	}

	@Override//5
	public Fridge getFridge(String fridgeName) {
		
		return fridgeDAO.getFridge(fridgeName);
	}

	//add the constraint that fridge can only hold 3 food items
	@Override//6
	public boolean addFoodItem(Fridge fridge, FoodItem foodItem) {
		List<FoodItem> fItem = fridgeDAO.getFoodItems(fridge);
		if(fItem.size()>2) {
		return false;
		}
		return fridgeDAO.addFoodItem(fridge, foodItem);
	}
	@Override//7
	public boolean addHealthInspector(String healthInspectorUsername, Fridge fridge) {
		
		return fridgeDAO.addHealthInspector(healthInspectorUsername, fridge);
	}
	@Override//8
	public List<FoodItem> getFoodItems(Fridge fridge) {
		
		return fridgeDAO.getFoodItems(fridge);
	}
	@Override//9
	public int deleteFoodItems(Fridge fridge) {
		
		return fridgeDAO.deleteFoodItems(fridge);
	}

	@Override//10
	public int deleteFoodItem(Fridge fridge, String foodItem) {
		
		return fridgeDAO.deleteFoodItem(fridge, foodItem);
	}

	

	@Override//11
	public List<HealthInspector> getHealthInspector(Fridge fridge) {
		
		return fridgeDAO.getHealthInspectors(fridge);
	}

	@Override//12
	public List<Fridge> getHealthInspectorFridges(String username) {
		
		return fridgeDAO.getHealthInspectorFridges(username);
	}

	

}
