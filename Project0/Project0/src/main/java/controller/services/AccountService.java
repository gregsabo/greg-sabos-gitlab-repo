package controller.services;

import model.Account;

public interface AccountService {
	public boolean saveAccount (Account account);
	
	public Account getAccount(String user, String password);
}
