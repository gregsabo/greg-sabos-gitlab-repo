package controller.dao;

import model.Account;

public interface AccountDAO {
	public boolean saveAccount(Account account);
	public Account getAccount(String user, String password);
}
