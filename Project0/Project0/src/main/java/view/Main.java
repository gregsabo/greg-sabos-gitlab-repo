package view;

import java.util.Scanner;

import controller.dao.ConnectionFactory;

public class Main {
	static Scanner in = new Scanner(System.in);
	
	public static void main(String[] args) {
		//get users choice and use it for the switch
		String choice;
		do {
			System.out.println("(a)enter your username and password. (b)register. (c)exit");
			choice = in.next();
			switch(choice) {
				case "a":
				case "A":
					//call the Login class and method here
					Login.login();
					break;
				case "b":
				case "B":
					//call the register class and method here
					Register.registerMenu();
					break;
				case "c":
					System.out.println("ok exiting....");
//					System.exit(0);
					break;
				default:
					System.out.println("invalid entry");
					break;
			}
		}while(!choice.equals("c"));
		
		ConnectionFactory.closeConnection();
		//close scanner
		in.close();
	}
}
