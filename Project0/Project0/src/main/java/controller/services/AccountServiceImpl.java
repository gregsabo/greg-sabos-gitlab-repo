package controller.services;

import controller.dao.AccountDAO;
import controller.dao.AccountDAOImpl;
import model.Account;

public class AccountServiceImpl implements AccountService {

	AccountDAO accountDAO = new AccountDAOImpl();
	@Override
	public boolean saveAccount(Account account) {
		
		return accountDAO.saveAccount(account);
	}


	@Override
	public Account getAccount(String user, String password) {
		// TODO Auto-generated method stub
		return accountDAO.getAccount(user, password);
	}

}
