package controller.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Account;
import model.FoodItem;
import model.Fridge;
import model.HealthInspector;

public class FridgeDAOImpl implements FridgeDAO {

	@Override
	public boolean saveFridge(Fridge fridge) {//1 boolean
		Connection connection = null;
		PreparedStatement stmt = null;
		int success = 0;
		try {
			connection = ConnectionFactory.getConnection();
			String sql = "INSERT INTO refrigerator(fridge_name, fridge_owner) VALUES(?,?)";
			
			stmt = connection.prepareStatement(sql);
			
			stmt.setString(1, fridge.getFridgeName());
			stmt.setInt(2, ConnectionFactory.getAccountLoggedIn().getId());
			success = stmt.executeUpdate();

		} catch (SQLException e) {
				e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			}catch (SQLException e) {
					e.printStackTrace();
			}
		}

		if (success == 0) {
			
			return false;
		} else
			return true;
	}

	@Override
	public boolean deleteFridge(String fridgeName) {//2 boolean
		Connection connection = null;
		PreparedStatement stmt = null;
		int success = 0;
		try {
			connection = ConnectionFactory.getConnection();
			Account account = ConnectionFactory.getAccountLoggedIn();
			String sql = "DELETE FROM refrigerator WHERE fridge_name = ? AND fridge_owner = ?";
			// Setup PreparedStatement
			stmt = connection.prepareStatement(sql);
			// Add parameters from account into PreparedStatement
			stmt.setString(1, fridgeName);
			stmt.setInt(2, account.getId());

			success = stmt.executeUpdate();

		} catch (SQLException e) {
				e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
					e.printStackTrace();
			}
		}

		if (success == 0) {
			
			return false;
		} else
			return true;
	}

	@Override
	public boolean updateFridge(Fridge fridge) {//3 future implementation
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Fridge> getAllFridges() {//4 list fridge
		List<Fridge> fridges = new ArrayList<>();
		Connection connection = null;
		Statement stmt = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.createStatement();
			String sql = "SELECT * FROM refrigerator WHERE fridge_owner = "
					+ ConnectionFactory.getAccountLoggedIn().getId();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Fridge fridge = new Fridge(rs.getString("fridge_name"), rs.getInt("fridge_owner"));
				fridges.add(fridge);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return fridges;
	}

	@Override
	public Fridge getFridge(String fridgeName) {//5 get specific fridge by name. Fridge class return
		Connection connection = null;
		Statement stmt = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.createStatement();
			String sql = "SELECT * FROM refrigerator WHERE fridge_name =  \'" + fridgeName + "'";
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				Fridge fridge = new Fridge();
				fridge.setFridgeId(rs.getInt("id"));
				fridge.setFridgeName(rs.getString("fridge_name"));
				fridge.setOwnerId(rs.getInt("fridge_owner"));
				return fridge;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public boolean addFoodItem(Fridge fridge, FoodItem foodItem) {//6 boolean
		Connection connection = null;
		PreparedStatement stmt = null;
		int success = 0;
		try {
			connection = ConnectionFactory.getConnection();
			String sql = "INSERT INTO food_item(food_item_name, fridge_id) VALUES(?,?)";
			// Setup PreparedStatement
			stmt = connection.prepareStatement(sql);
			// Add parameters from account into PreparedStatement
			stmt.setString(1, foodItem.getFoodName());
			stmt.setInt(2, fridge.getFridgeId());

			success = stmt.executeUpdate();

		} catch (SQLException e) {
				e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
					e.printStackTrace();
			}
		}

		if (success == 0) {
			// then update didn't occur, throw an exception
			// throw new Exception("Insert food item failed: " + foodItem.getFoodName());
			return false;
		}
		return true;
	}

	@Override
	public boolean addHealthInspector(String healthInspectorUsername, Fridge fridge) {//7 boolean
		Connection connection = null;
		PreparedStatement stmt = null;
		int success = 0;
		try {
			connection = ConnectionFactory.getConnection();
			Statement stm = connection.createStatement();
			StringBuilder str = new StringBuilder();
			str.append("SELECT users.id ");
			str.append("FROM users ");
			str.append("INNER JOIN account_type ");
			str.append("ON users.account_type_id = account_type.id ");
			str.append("WHERE user_name = \'" + healthInspectorUsername + "\'");

			ResultSet rSet = stm.executeQuery(str.toString());

			if (rSet.next()) {
				int userID = rSet.getInt(1);
				String sql = "INSERT INTO refrigerator_health_inspectors VALUES(?, ?)";
				// Setup PreparedStatement
				stmt = connection.prepareStatement(sql);
				stmt.setInt(1, fridge.getFridgeId());
				stmt.setInt(2, userID);
				success = stmt.executeUpdate();
			}

		} catch (SQLException e) {
				e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
					e.printStackTrace();
			}
		}

		if (success == 0) {
			
			return false;
		}
		return true;
	}

	@Override
	public List<FoodItem> getFoodItems(Fridge fridge) {// 8 list foodItem
		List<FoodItem> foodItems = new ArrayList<>();
		Connection connection = null;
		Statement stmt = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.createStatement();
			String sql = "SELECT * FROM food_item WHERE fridge_id = " + fridge.getFridgeId();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				foodItems.add(new FoodItem(rs.getString("food_item_name")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return foodItems;
	}
	
	@Override
	public int deleteFoodItems(Fridge fridge) {	//9 int
		int rowsUpdated = 0;		
		try {
			Connection conn = ConnectionFactory.getConnection();
			StringBuilder str = new StringBuilder();
			str.append("DELETE FROM food_item WHERE fridge_id = ?");
			PreparedStatement stm = conn.prepareStatement(str.toString());
			stm.setInt(1, fridge.getFridgeId());
			rowsUpdated = stm.executeUpdate();			
			
		} catch (SQLException e) {
				e.printStackTrace();
		}		
		return rowsUpdated;
	}
	
	@Override
	public int deleteFoodItem(Fridge fridge, String foodItem) {//10  int
		int rowsUpdated = 0;		
		try {
			Connection conn = ConnectionFactory.getConnection();
			StringBuilder str = new StringBuilder();
			str.append("DELETE FROM food_item WHERE fridge_id = ? AND food_item_name = ?");
			PreparedStatement stm = conn.prepareStatement(str.toString());
			stm.setInt(1, fridge.getFridgeId());
			stm.setString(2, foodItem);
			rowsUpdated = stm.executeUpdate();			
		} catch (SQLException e) {
				e.printStackTrace();
		}				
		return rowsUpdated;						
	}

	@Override
	public List<HealthInspector> getHealthInspectors(Fridge fridge) {//11 list of health inspectors
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT users.id, users.user_name, users.user_password, users.account_type_id FROM users ");
		sql.append("INNER JOIN refrigerator_health_inspectors ");
		sql.append("ON users.id = refrigerator_health_inspectors.health_inspector_id ");
		sql.append("WHERE refrigerator_health_inspectors.refrigerator_id = " + fridge.getFridgeId());
		List<HealthInspector> healthInspectors = new ArrayList<>();
		Connection connection = null;
		Statement stmt = null;
		try {
			connection = ConnectionFactory.getConnection();
			stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(sql.toString());
			while (rs.next()) {
				HealthInspector account = new HealthInspector(rs.getString("user_name"), rs.getString("user_password"),
						rs.getInt("account_type_id"));
				healthInspectors.add(account);
			}
		} catch (SQLException e) {
				e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (SQLException e) {
					e.printStackTrace();
			}
		}
		return healthInspectors;
	}

	@Override
	public List<Fridge> getHealthInspectorFridges(String username) {//12 list of fridges
		List<Fridge> fridges = new ArrayList<>();

		StringBuilder st = new StringBuilder();

		st.append("SELECT  refrigerator.id, refrigerator.fridge_name, refrigerator.fridge_owner ");
		st.append("FROM refrigerator ");
		st.append("INNER JOIN refrigerator_health_inspectors ");
		st.append("ON refrigerator.id = refrigerator_health_inspectors.refrigerator_id ");
		st.append("INNER JOIN users ");
		st.append("ON users.id = refrigerator_health_inspectors.health_inspector_id ");
		st.append("WHERE users.user_name = '" + username + "' ");

		Connection conn;
		Statement stm = null;

		try {
			conn = ConnectionFactory.getConnection();
			if (conn != null) {
				stm = conn.createStatement();
				ResultSet res = stm.executeQuery(st.toString());
				while (res.next()) {
					Fridge fridge = new Fridge(res.getString("fridge_name"), res.getInt("fridge_owner"));
					fridges.add(fridge);
				}
			}
		} catch (SQLException e) {
				e.printStackTrace();
		} finally {
			if (stm != null) {
				try {
					stm.close();
				} catch (SQLException e) {
						e.printStackTrace();
				}
			}
		}

		return fridges;
	}
	
}
