DROP TABLE users;
DROP TABLE account_type;
DROP TABLE food_item;
DROP TABLE refrigerator;
DROP TABLE refrigerator_health_inspectors;


CREATE TABLE account_type(
	  id serial PRIMARY KEY
	, account_type varchar(100) NOT NULL unique
);

CREATE TABLE users(
	  id serial PRIMARY KEY
	, user_name varchar(100) NOT NULL UNIQUE
	, user_password varchar(100) NOT NULL 
	, account_type_id integer NOT NULL 
	, CONSTRAINT account_type_constraint FOREIGN KEY(account_type_id) REFERENCES account_type(id) ON DELETE CASCADE
);


CREATE TABLE refrigerator(
	id serial PRIMARY KEY
	, fridge_name varchar(100)  NOT NULL UNIQUE
	, fridge_owner integer NOT NULL 
	, CONSTRAINT fridge_owner_constraint FOREIGN KEY(fridge_owner) REFERENCES users(id) ON DELETE CASCADE
);


CREATE TABLE food_item(
	id serial PRIMARY KEY
	, food_item_name varchar(100) NOT NULL
	, fridge_id integer NOT NULL
	, CONSTRAINT food_item_constraint FOREIGN KEY(fridge_id) REFERENCES refrigerator(id) ON DELETE CASCADE
);


CREATE TABLE refrigerator_health_inspectors(--junction TABLE WITH composite PRIMARY key
	refrigerator_id integer REFERENCES refrigerator(id) ON DELETE CASCADE 
	, health_inspector_id integer REFERENCES users(id) ON DELETE CASCADE
	, CONSTRAINT composite_refrigerator_health_insp_key PRIMARY KEY(refrigerator_id, health_inspector_id)
);



INSERT INTO account_type(account_type) values('restaurant');
INSERT INTO users(user_name, user_password, account_type_id) VALUES('healthInsp1', '123',  1);
INSERT INTO refrigerator(fridge_name, fridge_owner) VALUES('f 1',  1);
INSERT INTO food_item(food_item_name, fridge_id) VALUES('cheese', 8);


SELECT users.id 
FROM users 
INNER JOIN account_type
ON users.account_type_id = account_type.id
WHERE user_name = 'healthInsp2';



SELECT * FROM users WHERE user_name = 'eduar' AND user_password = '123';
DELETE FROM refrigerator WHERE fridge_name = 'f 1';

SELECT * FROM account_type;
SELECT * FROM users;
SELECT * FROM refrigerator;
SELECT * FROM food_item;
SELECT * FROM refrigerator_health_inspectors;

DELETE FROM food_item WHERE fridge_id = 8;
DELETE FROM food_item WHERE fridge_id = 8 AND food_item_name = 'chicken';

SELECT users.user_name, users.user_password, account_type.account_type  
FROM users 
RIGHT OUTER JOIN account_type 
ON users.account_type_id = account_type.id;


SELECT food_item.food_item_name, refrigerator.fridge_name
FROM food_item
full outer JOIN refrigerator 
ON food_item.fridge_id = refrigerator.id;

DELETE FROM refrigerator WHERE fridge_name = 'eduarFridge1' AND fridge_owner = 3;

SELECT users.id, users.user_name, users.user_password, users.account_type_id FROM users  
INNER JOIN refrigerator_health_inspectors
ON users.id = refrigerator_health_inspectors.health_inspector_id
WHERE refrigerator_health_inspectors.refrigerator_id = 11;

SELECT  refrigerator.id, refrigerator.fridge_name, refrigerator.fridge_owner 
FROM refrigerator
INNER JOIN refrigerator_health_inspectors
ON refrigerator.id = refrigerator_health_inspectors.refrigerator_id 
INNER JOIN users
ON users.id = refrigerator_health_inspectors.health_inspector_id 
WHERE users.user_name = 'healthInsp1';
