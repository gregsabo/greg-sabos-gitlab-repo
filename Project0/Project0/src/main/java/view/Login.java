package view;

import controller.dao.ConnectionFactory;
import controller.services.AccountService;
import controller.services.AccountServiceImpl;
import model.Account;

public class Login {
	public static void login() {
		//get user info
		System.out.println("Enter username: ");
		String username = Main.in.next();
		System.out.println("Enter password: ");
		String password = Main.in.next();
		
		//take the user input and now send it to service layer
		AccountService accountService = new AccountServiceImpl();
		//take input and get account using account getter
		Account account = accountService.getAccount(username,password);
		
		if(account !=null) {
			System.out.println("success");
			//call the database and set the login name
			ConnectionFactory.setAccountLoggedIn(account);
			
			//take the account and call the get method for account type
			//make sure db has id = 1 to inspector
			if(account.getAccountTypeId() ==1) {
				HealthInspectorView.healthInspectorMenu();
			}
			//make sure the db has id 2 = to restaurant owner
			else if(account.getAccountTypeId()==2) {
				RestaurantView.restaurantMenu();
			}
		}
		else {
			System.err.println("Wrong username or password");
		}
	}
}
