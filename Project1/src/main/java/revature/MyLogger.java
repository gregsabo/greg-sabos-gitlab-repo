package revature;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class MyLogger {
	
	private static Level logLevel = Level.ALL;
	
	
	public static Logger getLoggerForClass(Object clazz) {
		
		Logger myLogger = Logger.getLogger(clazz.getClass());
		myLogger.setLevel(logLevel);
		
		return myLogger;
	}
	
	
	public static <T> Logger getLoggerForClass(Class<T> clazz) {
		Logger myLogger = Logger.getLogger(clazz);
		myLogger.setLevel(logLevel);
		
		return myLogger;
	}
}
