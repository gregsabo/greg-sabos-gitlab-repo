package controller.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Account;

public class AccountDAOImpl implements AccountDAO {
	@Override
	public boolean saveAccount(Account account) {
	Connection connection = null;
	PreparedStatement stmt = null;
	int success = 0;
	
	try {
		connection = ConnectionFactory.getConnection();
		String sql = "INSERT INTO users(user_name, user_password, account_type_id) VALUES(?,?,?)";
		
		stmt = connection.prepareStatement(sql);
		
		stmt.setString(1, account.getUser());
		stmt.setString(2, account.getPassword());
		stmt.setInt(3, account.getAccountTypeId());
		success = stmt.executeUpdate();
	}catch(SQLException e) {
		e.printStackTrace();
	}finally {
		try {
			if(stmt != null)
				stmt.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	if(success ==0) {
		return false;
	}else {
		return true;
	}
	
	}


	@Override
	public Account getAccount(String user, String password) {
		Connection connection = null;
		Statement stmt = null;
		
		try {
			connection = ConnectionFactory.getConnection();
			
			stmt =connection.createStatement();
			
			String sql = "SELECT * FROM users WHERE user_name = \'" + user + "' "
					+ "AND user_password = \'" + password +"'";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			if(rs.next()) {
				Account account = new Account();
				account.setId(rs.getInt("id"));
				account.setUser(rs.getString("user_name"));
				account.setPassword(rs.getString("user_password"));
				account.setAccountTypeId(rs.getInt("account_type_id"));
				return account;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
