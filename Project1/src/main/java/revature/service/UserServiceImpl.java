package revature.service;

import revature.dao.UserDao;
import revature.dao.UserDaoImpl;
import revature.model.User;

public class UserServiceImpl implements UserService {
	
	private UserDao userDao;
	
	{
		userDao = new UserDaoImpl();
	}
	
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public User selectUserByUsername(String username) {
		User targetUser = userDao.selectUserByUsername(username);
		return targetUser;
	}
}
