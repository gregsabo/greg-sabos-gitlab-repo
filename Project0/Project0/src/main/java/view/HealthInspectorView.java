package view;

import controller.services.AccountService;
import controller.services.AccountServiceImpl;
import model.Account;

public class HealthInspectorView {
	
	public static void addHealthInspectorUser() {
		//login in this method instead of Login.java
		System.out.println("Enter username: ");
		String username = Main.in.next();
		System.out.println("Enter password: ");
		String password = Main.in.next();
		
		//1 is health inspector account type id
		Account healthInspectorUser = new Account(username, password, 1);
		AccountService accountService = new AccountServiceImpl();
		
		if(accountService.saveAccount(healthInspectorUser)) {
			System.out.println("saved to DB");
		}else {
			System.err.println("user exist already");
		}
	}
	public static void healthInspectorMenu() {
		String choice;
		System.out.println("Welcome Health Inspector!");
		do {
			System.out.println("(a)view fridges you have access to (b)select specific fridge"
					+ "(view contents, remove food) (c)exit");
            choice = Main.in.nextLine();
            switch(choice) {
	            case "a":
	            case "A":
	            	FridgeView.displayHealthInspectorFridges();
	            	break;
	            case "b":
	            case "B":
	            	FridgeView.selectFridge(1);
	            	break;
	            case "c":
//	            	System.exit(0);
	            	System.out.println("exiting...");
	            	break;
	            default:
	            	System.out.println("invalid entry...");
	            	break;
            }
		}while(!choice.equals("c"));    
	}
}
