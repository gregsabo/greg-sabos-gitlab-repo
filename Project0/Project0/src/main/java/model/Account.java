package model;

public class Account {
	private int id;
	private int accountTypeId;
	private String user;
	private String password;
	
	public Account() {
		
	}
	public Account(String user, String password, int accountTypeId) {
		this.user = user;
		this.password = password;
		this.accountTypeId = accountTypeId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAccountTypeId() {
		return accountTypeId;
	}
	public void setAccountTypeId(int accountTypeId) {
		this.accountTypeId = accountTypeId;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
