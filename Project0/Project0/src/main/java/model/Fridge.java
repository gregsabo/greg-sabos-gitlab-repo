package model;

public class Fridge {
	private int fridgeId;
	private String fridgeName;
	private int ownerId;
	
	public Fridge() {
		
	}

	public Fridge(String fridgeName, int ownerId) {
		super();
		this.fridgeName = fridgeName;
		this.ownerId = ownerId;
	}

	public int getFridgeId() {
		return fridgeId;
	}

	public void setFridgeId(int fridgeId) {
		this.fridgeId = fridgeId;
	}

	public String getFridgeName() {
		return fridgeName;
	}

	public void setFridgeName(String fridgeName) {
		this.fridgeName = fridgeName;
	}

	public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	
	
}
