package view;

import controller.services.AccountService;
import controller.services.AccountServiceImpl;
import model.Account;

public class RestaurantView {
	static void addRestaurantUser() {
		//login in this method instead of Login.java
		System.out.println("Enter username: ");
		String username = Main.in.next();
		System.out.println("Enter password: ");
		String password = Main.in.next();
		
		//add new account with credentials
		Account restaurantUser = new Account(username, password, 2);
		//tie in the service layer
		//interface                            class
		AccountService accountService = new AccountServiceImpl();
		
		//tRYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
		if(accountService.saveAccount(restaurantUser)) {
			System.out.println("saved successfully");
		}else {
			System.out.println("user already exist");
		}
		
	}
	static void restaurantMenu() {
		String choice;
		System.out.println("welcome restaurant owner");
		do {
		System.out.println("Restaurant Menu");
		System.out.println("(a)add fridge. (b)remove fridge. (c)view fridge. (d)select"
				+ " fridge (add, remove, transfer after selecting) (e)exit");
		//(c)add food.(d)remove food. (e)transfer food to another fridge.
		choice = Main.in.next();
			switch(choice) {
				case "a":
				case "A":
					FridgeView.addFridge();
					break;
				case "b":
				case "B":
					FridgeView.deleteFridge();
					break;
				case "c":
				case "C":
					FridgeView.displayRestaurantFridges();
					break;
				case "d":
				case "D":
					//2 is hard coded to be restaurant owner account type
					FridgeView.selectFridge(2);
					break;
				case "e":
					System.out.println("exiting...");
					break;
				default:
					System.out.println("invalid entry...");	
					break;
			}
		}while(!choice.equals("e"));
	}
	
}
