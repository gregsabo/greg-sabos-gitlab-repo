package controller.services;

import java.util.List;

import model.FoodItem;
import model.Fridge;
import model.HealthInspector;

public interface FridgeService {
	boolean saveFridge(Fridge fridge);//1
	boolean deleteFridge(String fridgeName);//2
	boolean updateFridge(Fridge fridge);//3
	
	public List<Fridge> getAllFridges();//4
	
	public Fridge getFridge(String fridgeName);//5
	
	public boolean addFoodItem(Fridge fridge, FoodItem foodItem);//6
	
	
	public boolean addHealthInspector(String healthInspectorUsername, Fridge fridge);//7
	
	public List<FoodItem> getFoodItems(Fridge fridge);//8
	
	public int deleteFoodItems(Fridge fridge);//9
	
	public int deleteFoodItem(Fridge fridge, String foodItem);//10
	
	
	
	
	public List<HealthInspector> getHealthInspector(Fridge fridge);//11
	
	public List<Fridge> getHealthInspectorFridges(String username);
}
