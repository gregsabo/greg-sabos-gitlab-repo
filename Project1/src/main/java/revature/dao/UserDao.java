package revature.dao;

import java.util.List;

import revature.model.User;

public interface UserDao {

	// Insert methods
	boolean insertUser(User newUser);
	
	// Read methods
	List<User> selectAllUsers();
	User selectUserByUsername(String username);

	User selectUser(String string, String string2);

	
	
}
