package model;

public class FoodItem {
	String foodName;
	
	public FoodItem(String foodName) {
		this.foodName = foodName;
	}

	//accessor
	public String getFoodName() {
		return foodName;
	}
	//mutator
	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}
	
}
