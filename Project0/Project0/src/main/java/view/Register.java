package view;

public class Register {
	public static void registerMenu() {
		System.out.println("Welcome to the registration page!");
		
		String option;
		do {
			System.out.print("Enter (h) if you are a health "
					+ "inspector and (r) if you are a restaurant owner:");
			option = Main.in.next();
			switch(option) {
				case "h":
				case "H":
					//register new retaurant
					RestaurantView.addRestaurantUser();
					break;
				case "r":
				case "R":
					//register new health inspector
					HealthInspectorView.addHealthInspectorUser();
					break;
				case "e":
					System.out.println("exiting...");
					break;
				default:
					System.out.println("invalid entry...");
					break;
			}
		}while(!option.equals("0"));	
		
	}
}
