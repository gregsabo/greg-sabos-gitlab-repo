package revature.service;

import java.util.List;

import revature.model.User;

public interface UserService {
	
	User selectUserByUsername(String username);
	
}
