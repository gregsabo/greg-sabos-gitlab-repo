package controller.dao;

import java.util.List;

import model.FoodItem;
import model.Fridge;
import model.HealthInspector;

public interface FridgeDAO {
	//11 behaviors
	boolean saveFridge(Fridge fridge);
	boolean deleteFridge(String fridgeName);
	boolean updateFridge(Fridge fridge);
	
	public List<Fridge> getAllFridges();//4
	public Fridge getFridge(String fridgeName);//5
	
	public boolean addFoodItem(Fridge fridge, FoodItem foodItem);//6
	public boolean addHealthInspector(String healthInspectorUsername, Fridge fridge);
	
	public List<FoodItem> getFoodItems(Fridge fridge);//8
	
	public int deleteFoodItems(Fridge fridge);//99
	public int deleteFoodItem(Fridge fridge, String foodItem);//10
	
	public List<HealthInspector> getHealthInspectors(Fridge fridge);
	public List<Fridge> getHealthInspectorFridges(String username);
	
}
